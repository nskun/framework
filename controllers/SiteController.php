<?php
class SiteController extends Controller {
    public function indexAction() {
        $user = $this->session->get('user');
        !isset($user['kengen']) && $user['kengen'] = 0;
        $site = $this->db_manager->get('Site')->fetchAllCreatedSiteByKengen($user['kengen']);
        return $this->render(array(
            'sites' => $site,
            '_token' => $this->generateCsrfToken('site/post'),
        ));
    }
}