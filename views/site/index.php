<?php $this->setLayoutVar('title', 'ホーム'); ?>
<h2>ホーム</h2>
<form action="<?php echo $base_url; ?>/status/post" method="post">
    <input type="hidden" name="_token" value="<?php echo $this->escape($_token); ?>" />
    <?php // Todo: エラー処理 ?>
</form>

<div id="sites">
    <?php foreach($sites AS $site): ?>
    <div class="sites">
        <?php echo $this->escape($site['name']); ?>
        <?php echo $this->escape($site['url']); ?>
        <?php echo $this->escape($site['setumei']); ?>
    </div>
    <?php endforeach; ?>
</div>