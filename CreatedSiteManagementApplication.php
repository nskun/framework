<?php
class CreatedSiteManagementApplication extends Application {
    protected $login_action = array('account', 'signin');

    public function getRootDir() {
        return dirname(__FILE__);
    }

    protected function registerRoutes() {
        return array(
            '/'
                => array('controller' => 'site', 'action' => 'index'),
            '/status/post'
                => array('controller' => 'site', 'action' => 'post'),
            '/account'
                => array('controller' => 'account', 'action' => 'index'),
            '/account/:action'
                => array('controller' => 'account'),
        );
    }

    protected function configure() {
        $this->db_manager->connect('master', array(
            'dsn'      => 'mysql:dbname=top;host=localhost',
            'user'     => 'root',
            'password' => 'defacto',
        ));
    }
}