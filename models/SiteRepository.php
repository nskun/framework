<?php
class SiteRepository extends DbRepository {
    public function insert($name, $url, $setumei, $kengen) {
        $sql = "
        INSERT INTO site(name, url, setumei, kengen)
          VALUE(:name, :url, :setumei, :kengen)
        ";
        $stmt = $this->execute($sql, array(
            ':name'    => $name,
            ':url'     => $url,
            ':setumei' => $setumei,
            ':kengen'  => $kengen,
        ));
    }
    public function fetchAllCreatedSiteByKengen($kengen) {
        $sql = "
        SELECT s.name, s.url, s.setumei
         FROM site AS s
         WHERE s.kengen <= :kengen
         ORDER BY s.id
        ";
        return $this->fetchAll($sql, array(':kengen' => $kengen));
    }
}