INSERT INTO site (name, url, setumei, kengen) VALUES
  ('Rundec'       ,'http://tk2-230-24751.vs.sakura.ne.jp:4440/'                                , 'ジョブスケジューラー', 1),
  ('Twitter'      ,'http://tk2-230-24751.vs.sakura.ne.jp/twitter_bot/DownloadMain.php'         , 'Twitter開発用URL1', 1),
  ('Twitter'      ,'http://tk2-230-24751.vs.sakura.ne.jp/twitter_bot/write_the_update_file.php', 'Twitter開発用URL2', 1),
  ('Twitter'      ,'http://tk2-230-24751.vs.sakura.ne.jp/twitter_bot/post_twitter.php'         , 'Twitter開発用URL3', 1),
  ('Twitter'      ,'https://twitter.com/it_system_news'                                        , '開発環境：PHP + Redis + Rundeck', 0),
  ('Brainfuck'    ,'http://tk2-230-24751.vs.sakura.ne.jp/brainfuck/brainfuck.html'             , '開発環境：PHP + JavaScript + MySQL + HTML/CSS', 0),
  ('BrainfuckAPI' ,'http://tk2-230-24751.vs.sakura.ne.jp/brainfuck/php/rest-api/rest.php'      , '開発環境：PHP + MySQL', 0),
  ('Github'       ,'https://github.com/nskun/'                                                 , '※更新停止', 0),
  ('Bitbucket'    ,'https://bitbucket.org/nskun/'                                              , '※Bitbucket', 0),
  ('GitBucket'    ,'http://tk2-230-24751.vs.sakura.ne.jp:8080/gitbucket/'                      , 'GitBucket', 1),
  ('Munin'        ,'http://tk2-230-24751.vs.sakura.ne.jp/munin/'                               , 'サーバ監視', 1);
