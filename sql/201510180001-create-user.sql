CREATE TABLE `user` (
  `id`          int          NOT NULL AUTO_INCREMENT,
  `name`        varchar(255) NOT NULL,
  `password`    varchar(255) NOT NULL,
  `kengen`      tinyint NOT NULL,
  `update_date` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_date` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 MAX_ROWS=200000;

